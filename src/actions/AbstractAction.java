package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import entities.IEntity;
import managers.AbstractManager;
import managers.AbstractManagerWithName;

public abstract class AbstractAction{

	public static <T extends IEntity> void showAll (HttpServletRequest request, AbstractManager<T> manager, String param) {
		ArrayList<T> obj = manager.getAll();
		request.setAttribute(param, obj);
	}
	
	public static <T extends IEntity> void showById (HttpServletRequest request, int id, AbstractManager<T> manager, String param){
		T obj = manager.getById(id);
		request.setAttribute(param, obj);
	}
	
	public static <T extends IEntity> void showByName (HttpServletRequest request, String name, AbstractManagerWithName<T> manager, String param){
		ArrayList<T> obj = manager.getByName(name);
		request.setAttribute(param, obj);
	}
}
