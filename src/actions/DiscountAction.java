package actions;

import javax.servlet.http.HttpServletRequest;

import entities.Discount;
import managers.DiscountManager;
import services.R;

public class DiscountAction extends AbstractAction {
	public static void showAll (HttpServletRequest request){
		AbstractAction.<Discount>showAll(request, new DiscountManager(), R.listDiscount);
	}
}
