package actions;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.ETitle;
import entities.User;
import managers.CookieManager;
import managers.PasswordManager;
import managers.SessionManager;
import managers.UserManager;
import services.AuthorizationChecker;
import services.ConnectionBDD;
import services.R;

public class UserAction extends AbstractAction {
	public static void showAll(HttpServletRequest request) {
		AbstractAction.<User>showAll(request, new UserManager(), R.listUser);
	}

	public static void showById(HttpServletRequest request, int id) {
		AbstractAction.<User>showById(request, id, new UserManager(), R.user);
	}

	/**
	 * Adds a user to the database
	 * 
	 * @author Guillaume
	 * @param request
	 */
	public static void addUser(HttpServletRequest request) {
		boolean added = false;
		User newUser = new User();
		UserManager userManager = new UserManager();

		try {
			newUser.setEmail(request.getParameter(R.authorizationLogin));
			if (userManager.getByField("email", newUser.getEmail()) != null) {
				SessionManager.setFlashMessage(
						request,
						R.errorSubscribe,
						"Cette email existe deja");
			} else {
				// Gets the variables from the request
				newUser.setFirstName(
						request.getParameter(R.authorizationFirstName));
				newUser.setLastName(
						request.getParameter(R.authorizationLastName));
				newUser.setTitle(
						ETitle.values()[Integer.parseInt(
								request.getParameter(R.authorizationTitle))]);

				// Ashes the password
				newUser.setPassword(PasswordManager.code(
						request.getParameter(R.authorizationPassword)));
				newUser.setPhoneHome(
						request.getParameter(R.authorizationPhoneHome));
				newUser.setPhonePortable(
						request.getParameter(R.authorizationPhonePortable));

				newUser = userManager.insert(newUser);
				// Adapts the session to the new user
				added = true;
				Connection co = ConnectionBDD.getConnection();
				co.commit();
				
			}
		} catch (Exception e) {
			SessionManager.setFlashMessage(
					request,
					R.errorSubscribe,
					"Une erreur est survenue veuillez reessayer plus tard");
		}

		if (added) {
			SessionManager.setAttribute(request, R.sessionUser, newUser);
		}
	}

	/**
	 * Connects the user after checking his login and password
	 * 
	 * @author Guillaume
	 * @param request
	 * @param response
	 */
	public static void connectUser(
			HttpServletRequest request,
			HttpServletResponse response) {
		AuthorizationChecker authorizationChecker = new AuthorizationChecker();
		// Gets all the users
		@SuppressWarnings("unchecked")
		ArrayList<User> users = (new UserManager()).getAll();

		// Variables
		String userLogin = request.getParameter(R.authorizationLogin);
		String userPassword =
				PasswordManager
						.code(request.getParameter(R.authorizationPassword));
		String cookieWanted = request.getParameter(R.cookieName);

		// Gets the user if it exists. If not, the variable is null
		User requestingUser =
				authorizationChecker.checkUser(users, userLogin, userPassword);

		int userID = 0;
		int cmp = 0;
		boolean isFound = false;
		while (cmp < users.size() && !isFound) {
			if (users.get(cmp).getEmail().equals(userLogin)) {
				userID = users.get(cmp).getId();
				isFound = true;
			} else {
				cmp++;
			}
		}

		if (requestingUser != null) {
			// Inserts the user in the session
			SessionManager.setAttribute(request, R.sessionUser, requestingUser);
			if (cookieWanted != null) {
				CookieManager.createCookie(
						R.cookieName,
						userID,
						userPassword,
						response);
			}

		}
	}

	/**
	 * Disconnects the user from the session and deletes the cookie
	 * 
	 * @author Guillaume
	 * @param request
	 * @param response
	 */
	public static void disconnectUser(
			HttpServletRequest request,
			HttpServletResponse response) {
		SessionManager.deleteSession(request);
		CookieManager.deleteCookie(
				R.cookieName,
				request,
				response);
	}
}
