package actions;

import javax.servlet.http.HttpServletRequest;

import entities.CommandView;
import managers.CommandViewManager;
import services.R;

public class CommandViewAction extends AbstractAction {

	public static void showAll (HttpServletRequest request){
		AbstractAction.<CommandView>showAll(request, new CommandViewManager(), R.listCommandView);
	}
	
	public static void showById (HttpServletRequest request, int id){
		AbstractAction.<CommandView>showById(request, id, new CommandViewManager(), R.commandView);
	}
	
}
