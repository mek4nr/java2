package actions;

import javax.servlet.http.HttpServletRequest;

import entities.TaxesCategory;
import managers.TaxesCategoryManager;
import services.R;

public class TaxesCategoryAction extends AbstractAction {
	public static void showAll (HttpServletRequest request){
		AbstractAction.<TaxesCategory>showAll(request, new TaxesCategoryManager(), R.listTaxe);
	}

}
	