package actions;

import javax.servlet.http.HttpServletRequest;

import entities.Category;
import managers.CategoryManager;
import services.R;

public class CategoryAction extends AbstractAction {
	
	public static void showAll (HttpServletRequest request){
		AbstractAction.<Category>showAll(request, new CategoryManager(), R.listCategory);
	}
}
