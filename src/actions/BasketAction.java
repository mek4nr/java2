package actions;

import javax.servlet.http.HttpServletRequest;

import entities.Basket;
import managers.ProductViewManager;
import managers.SessionManager;
import services.R;

public class BasketAction {

	public static void addToBasket(HttpServletRequest request){
		String idS = request.getParameter(R.idProduct);
		String qtyS = request.getParameter(R.quantity);
		
		int id = 0;
		int qty = 0;
		
		try{
			id = Integer.parseInt(idS);
			qty = Integer.parseInt(qtyS);
		}
		catch(Exception e){
			
		}
		
		if(qty == 0) qty = 1;
		
		if(id > 0){
			Basket c = (Basket) SessionManager.getAttribute(request, R.basket);
			
			if(c == null) c = new Basket();
				
			c.addToBasket((new ProductViewManager()).getByField("product_id", id), qty);
			
			SessionManager.setAttribute(request, R.basket, c);
		}
	}
	
	public static void setToBasket(HttpServletRequest request){
		String idS = request.getParameter(R.idProduct);
		String qtyS = request.getParameter(R.quantity);
		
		int id = 0;
		int qty = 0;
		
		try{
			id = Integer.parseInt(idS);
			qty = Integer.parseInt(qtyS);
		}
		catch(Exception e){
			
		}
		
		if(qty == 0) qty = 1;
		
		if(id > 0){
			Basket c = (Basket) SessionManager.getAttribute(request, R.basket);
			
			if(c == null) c = new Basket();
				
			c.setToBasket((new ProductViewManager()).getByField("product_id", id), qty);
			
			SessionManager.setAttribute(request, R.basket, c);
		}
	}
	
	public static void removeToBasket(HttpServletRequest request){
		String idS = request.getParameter(R.idProduct);
		String qtyS = request.getParameter(R.quantity);
		
		int id = 0;
		int qty = 0;
		
		try{
			id = Integer.parseInt(idS);
			qty = Integer.parseInt(qtyS);
		}
		catch(Exception e){
			
		}
		
		if(id > 0){
			Basket c = (Basket) SessionManager.getAttribute(request, R.basket);
			
			if(c == null) c = new Basket();
			
			if(qty != 0)
				c.removeToBasket((new ProductViewManager()).getByField("product_id", id), qty);
			else
				c.removeToBasket((new ProductViewManager()).getByField("product_id", id));
			
			SessionManager.setAttribute(request, R.basket, c);
		}
	}
}
