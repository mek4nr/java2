package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import entities.Product;
import managers.ProductManager;
import managers.SessionManager;
import services.R;

public class ProductAction extends AbstractAction {
	public static void showAll (HttpServletRequest request){
		AbstractAction.<Product>showAll(request, new ProductManager(), R.listProduct);
	}

	public static void showById (HttpServletRequest request, int id){
		AbstractAction.<Product>showById(request, id, new ProductManager(), R.product);
	}

	public static void showByName (HttpServletRequest request, String name){
		AbstractAction.<Product>showByName(request, name, new ProductManager(), R.listProduct);
	}

	public static void addProduct(HttpServletRequest request) {
		ProductManager manager = new ProductManager();

		String name = request.getParameter(R.formProductName);
		String description = request.getParameter(R.formProductDescription);
		String price = request.getParameter(R.formProductPrice);
		String stock = request.getParameter(R.formProductStock);
		String discount = request.getParameter(R.formProductDiscount);
		String taxe = request.getParameter(R.formProductTaxe);
		String category = request.getParameter(R.formProductCategory);

		ArrayList<String> errors = new ArrayList<>();


		if(name.equals("")) errors.add("Vous devez entrer un nom");
		if(price.equals("")) errors.add("Vous devez entrer un prix");
		if(stock.equals("")) errors.add("Vous devez entrer un stock");
		if(taxe == null || taxe.equals("")) errors.add("Vous devez choisir une taxe");
		if(category == null || category.equals("")) errors.add("Vous devez choisir une category");

		if(errors.size() > 0){
			SessionManager.setFlashMessage(request, R.errorAdress, errors);
			System.out.println(errors);
		} 
		else {
			try {
				Product p = new Product();
				p.setName(name);
				p.setDescription(description);
				p.setPrice(Float.parseFloat(price));
				p.setStock(Integer.parseInt(stock));
				p.setDiscount_id((discount == null || discount.equals("-1"))?null:Integer.parseInt(discount));
				p.setTaxesCategory_id(Integer.parseInt(taxe));
				p.setCategory_id(Integer.parseInt(category));
				
				manager.insert(p);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
