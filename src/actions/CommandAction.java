package actions;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;
import java.sql.SQLException;

import entities.Adress;
import entities.Basket;
import entities.Command;
import entities.CommandProducts;
import entities.ProductView;
import entities.User;
import managers.AdressManager;
import managers.CommandManager;
import managers.CommandProductsManager;
import managers.MailManager;
import managers.PasswordManager;
import managers.SessionManager;
import services.ConnectionBDD;
import services.R;

public class CommandAction extends AbstractAction {

	public static void showAll(HttpServletRequest request) {
		AbstractAction.<Command>showAll(request, new CommandManager(), R.listCommand);
	}

	public static void showById(HttpServletRequest request, int id) {
		AbstractAction.<Command>showById(request, id, new CommandManager(), R.command);
	}

	public static void addCommand(HttpServletRequest request) {
		String adress1 = request.getParameter(R.formAdressAdress1);
		String adress2 = request.getParameter(R.formAdressAdress2);
		String city = request.getParameter(R.formAdressCity);
		String country = request.getParameter(R.formAdressCountry);
		String postalCode = request.getParameter(R.formAdressPostalCode);
		String state = request.getParameter(R.formAdressState);
		Basket b = (Basket) SessionManager.getAttribute(request, R.basket);
		User user = (User) SessionManager.getAttribute(request, R.sessionUser);

		ArrayList<String> errors = new ArrayList<>();
		

		if(adress1.equals("")) errors.add("Vous devez entrer une adresse");
		if(city.equals("")) errors.add("Vous devez entrer une ville");
		if(country.equals("")) errors.add("Vous devez entrer un pays");
		if(postalCode.equals("")) errors.add("Vous devez entrer un code postal");
		if(state.equals("")) errors.add("Vous devez choisir une province");
		if(user == null) errors.add("Vous devez etre connecté pour créer une commande");
		if(b == null || b.getProducts().size() == 0) errors.add("Votre panier est vide");
		
		
		if(errors.size() > 0){
			SessionManager.setFlashMessage(request, R.errorAdress, errors);
			System.out.println(errors);
		} else {
			System.out.println(user.getPhoneHome());
			try {
				Connection co = ConnectionBDD.getConnection();
				co.setAutoCommit(false);
				try {
					// Creation adress
					Adress a = new Adress();
					a.setAdress1(adress1);
					a.setAdress2(adress2);
					a.setCity(city);
					a.setCountry(country);
					a.setPostalCode(postalCode);
					a.setState(state);
					a.setUser_id(user.getId());
					a = (new AdressManager()).insert(a);

					// Creation command
					Command c = new Command();
					c.setBilling_adress_id(a.getId());
					c.setDelivery_adress_id(a.getId());
					c.setSeed(PasswordManager.code(a.getId() + "aaa"));
					c.setStatus_id(1);
					c.setUser_id(user.getId());
					c = (new CommandManager()).insert(c);
					
					String message = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\r\n"
							+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n"
							+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" + "<head>\r\n"
							+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />\r\n"
							+ "<title>Votre Commande</title>\r\n" + "</head>\r\n" + "<body>\r\n"
							+ "	<p style=\"color:black\">" + "Bonjour, " + user.getFirstName() + " "
							+ user.getLastName() + "<br>"
							+ "Votre commande a bien été envoyée à l'adresse suivante :<br>" + a.getAdress1() + ", "
							+ a.getAdress2() + "<br>" + a.getCity() + ", " + a.getPostalCode() + ", " + a.getState()
							+ ", " + a.getCountry() + "<br><br>" + "Récapitulatif de votre commande :<br>";

					
					float total = 0;
					// ajout produits
					for (Map.Entry<ProductView, Integer> p : b.getProducts().entrySet()) {
						CommandProducts cp = new CommandProducts();
						cp.setCommand_id(c.getId());
						cp.setProduct_id(p.getKey().getProduct_id());
						cp.setQuantity(p.getValue());
						(new CommandProductsManager()).insert(cp);
						total += p.getKey().getVAD()*p.getValue();
						message += String.format("%s&emsp;%s&emsp;$%s<br/>", p.getKey().getProduct().getName(), p.getValue(), p.getKey().getVAD()*p.getValue());
					}
					co.commit();
					message += String.format("Total&emsp;$%s<br/>", total);
					
					try {
						String recipient = user.getEmail();
						String subject = "Clever Games - Confirmation de votre commande";
						
						
						message+= "</p>\r\n" + "</body>\r\n" + "</html>";

						MailManager.sendEmail(message, subject, recipient);
						SessionManager.setAttribute(request, R.basket, new Basket());
						
					} catch (Exception f) {
						f.printStackTrace();
					}

				} catch (Exception e) {
					e.printStackTrace();
					errors.add("Une erreur est survenue, veuillez réessayer plus tard");
					SessionManager.setFlashMessage(request, R.errorAdress, errors);
					co.rollback();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
