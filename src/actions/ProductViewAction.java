package actions;

import javax.servlet.http.HttpServletRequest;

import entities.ProductView;
import managers.ProductViewManager;
import services.R;

public class ProductViewAction extends AbstractAction {
	
	public static  void showAll (HttpServletRequest request){
		AbstractAction.<ProductView>showAll(request, new ProductViewManager(), R.listProductView);
	}
	
	public static  void showById (HttpServletRequest request, int id){
		ProductViewManager p = new ProductViewManager();
		ProductView obj = p.getByField("product_id", id);
		request.setAttribute(R.productView, obj);
	}
	
	public static void showByName (HttpServletRequest request, String name){
		request.setAttribute(R.listProductView, (new ProductViewManager()).getProductByName(name));
	}
}
