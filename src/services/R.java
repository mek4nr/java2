package services;

public final class R {
	public static String nameProduct = "p001";
	public static String idProduct = "p002";
	public static String idCommand = "p003";
	public static String idUser = "p004";
	public static String quantity = "p005";
	
	public static String user = "e001";
	public static String product = "e002";
	public static String productView = "e003";
	
	public static String basket = "e004";
	public static String command = "e005";
	public static String commandView = "e006";
	
	public static String listProduct = "l001";
	public static String listProductView = "l002";
	public static String listCommand = "l003";
	public static String listCommandView = "l004";
	public static String listUser = "l005";
	public static String listCategory = "1006";
	public static String listDiscount = "1007";
	public static String listTaxe = "1008";
	
	public static String authorizationLogin = "a001";
	public static String authorizationPassword = "a002";
	public static String authorizationTitle = "a003";
	public static String authorizationFirstName = "a004";
	public static String authorizationLastName = "a005";
	public static String authorizationPhoneHome = "a006";
	public static String authorizationPhonePortable = "a007";
	
	public static String sessionUser = "s001";
	public static String errorSubscribe = "eee1";
	public static String errorAdress = "eee2";
	
	public static String projectName = "java2";
	
	public static String cookieName = "c001";
	
	public static String formAdressAdress1 = "f001";
	public static String formAdressAdress2 = "f002";
	public static String formAdressCity = "f003";
	public static String formAdressCountry = "f004";
	public static String formAdressState = "f005";
	public static String formAdressPostalCode = "f006";
	
	public static String formProductName = "fp001";
	public static String formProductDescription = "fp002";
	public static String formProductPrice = "fp003";
	public static String formProductCategory = "fp004";
	public static String formProductStock = "fp005";
	public static String formProductDiscount = "fp006";
	public static String formProductTaxe = "fp007";
	
	public static String getProductLink(int id){
		return String.format("/%s/Product?%s=%s",projectName, R.idProduct, id);
	}
	
	public static String getProductsLink(){
		return String.format("/%s/Products",projectName, R.idProduct);
	}
	
	public static String getHomeLink(){
		return String.format("/%s/Home",projectName);
	}
	
	public static String getSetBasketLink(int id){
		return String.format("/%s/Basket/Set?%s=%s",projectName, R.idProduct, id);
	}
	
	public static String getAddBasketLink(int id){
		return String.format("/%s/Basket/Add?%s=%s",projectName, R.idProduct, id);
	}
	
	public static String getAddBasketLink(int id, int qty){
		return String.format("%s&%s=%s",getAddBasketLink(id), R.quantity, qty);
	}
	
	public static String getRemoveBasketLink(int id){
		return String.format("/%s/Basket/Remove?%s=%s",projectName, R.idProduct, id);
	}
	
	public static String getRemoveBasketLink(int id, int qty){
		return String.format("%s&%s=%s",getRemoveBasketLink(id), R.quantity, qty);
	}
	
	public static String getBasketLink(){
		return String.format("/%s/Basket",projectName);
	}
	
	public static String getAddProductLink(){
		return String.format("/%s/AddProduct",projectName);
	}
	
	public static String getCreateCommandLink(){
		return String.format("/%s/CreateCommandController",projectName);
	}
}
