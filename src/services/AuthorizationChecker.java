package services;

import java.util.ArrayList;

import entities.User;

public class AuthorizationChecker {
	/**
	 * Check if a user exists according to a login and a password
	 * 
	 * @author Guillaume
	 * @param usersList
	 *            List of the users
	 * @param login
	 *            Login submitted
	 * @param password
	 *            Password submitted
	 * @return User object or null if the user does not exists
	 */
	public User checkUser(
			ArrayList<User> usersList,
			String login,
			String password) {
		User result = null;
		boolean isFound = false;
		int cmp = 0;

		if (usersList != null) {
			while (!isFound && cmp < usersList.size()) {
				if (usersList.get(cmp).getEmail().equals(login)
						&& usersList.get(cmp).getPassword().equals(password)) {
					result = usersList.get(cmp);
					isFound = true;
				} else {
					cmp++;
				}
			}
		}

		return result;
	}

}
