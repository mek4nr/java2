package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public final class ConnectionBDD {
	private static final String url = "jdbc:mysql://localhost:3307/java2";
	private static final String login = "root";
	private static final String password = "abc123...";
	private static final String passwordQwerty = "qbc123...";
	public static Connection connection = null;

	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url, login, password);
		} catch (ClassNotFoundException | SQLException e) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(url, login, passwordQwerty);
			}
			catch (ClassNotFoundException | SQLException f) {
				f.printStackTrace();
			}
		}
		return connection;
	}

	public static void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static PreparedStatement prepareStatement(String query) {
		PreparedStatement retour = null;

		try {
			if (connection == null || connection.isClosed())
				getConnection();
			retour = connection.prepareStatement(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return retour;
	}
}
