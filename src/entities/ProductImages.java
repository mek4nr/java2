package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductImages extends AbstractEntity {
	private Integer product_id;
	private String src;

	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setProduct_id(line.getInt("product_id"));
			this.setSrc(line.getString("src"));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public static ArrayList<ProductImages> getDefault(int id){
		ArrayList<ProductImages> tt = new ArrayList<>();
		
		ProductImages p = new ProductImages();
		p.setProduct_id(id);
		p.setSrc("images/products/MCL1.JPG");
		
		tt.add(p);
		
		return tt;
	}

}
