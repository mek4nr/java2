package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Permission  extends AbstractEntity {
	private Integer id;
	private String codename;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodename() {
		return codename;
	}

	public void setCodename(String codename) {
		this.codename = codename;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCodename(line.getString("codename"));
			this.setId(line.getInt("id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
