package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class Taxe  extends AbstractEntity {
	private Integer id;
	private Integer percent;
	private String name;
	private Timestamp create_time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPercent() {
		return percent;
	}

	public void setPercent(Integer percent) {
		this.percent = percent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCreate_time(line.getTimestamp("create_time"));
			this.setId(line.getInt("id"));
			this.setName(line.getString("name"));
			this.setPercent(line.getInt("percent"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
