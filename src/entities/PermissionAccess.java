package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PermissionAccess  extends AbstractEntity {
	private Integer permission_id;
	private Permission permission;

	private Integer contentType_id;
	private ContentType contentType;

	private Integer actionTag_id;
	private ActionTag actionTag;

	public Integer getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(Integer permission_id) {
		this.permission_id = permission_id;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Integer getContentType_id() {
		return contentType_id;
	}

	public void setContentType_id(Integer contentType_id) {
		this.contentType_id = contentType_id;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public Integer getActionTag_id() {
		return actionTag_id;
	}

	public void setActionTag_id(Integer actionTag_id) {
		this.actionTag_id = actionTag_id;
	}

	public ActionTag getActionTag() {
		return actionTag;
	}

	public void setActionTag(ActionTag actionTag) {
		this.actionTag = actionTag;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setActionTag_id(line.getInt("actionTag_id"));
			this.setContentType_id(line.getInt("contentType_id"));
			this.setPermission_id(line.getInt("permission_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
