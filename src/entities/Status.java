package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Status  extends AbstractEntity {
	private Integer id;
	private String name;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setDescription(line.getString("description"));
			this.setId(line.getInt("id"));
			this.setName(line.getString("name"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
