package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import managers.ProductManager;

public class PackageProducts  extends AbstractEntity {
	private Integer package_id;
	private Product gamePackage;

	private Integer product_id;
	private Product product;

	private Integer quantity;

	public Integer getPackage_id() {
		return package_id;
	}

	public void setPackage_id(Integer package_id) {
		this.package_id = package_id;
		gamePackage = null;
	}

	public Product getGamePackage() {
		gamePackage = foreignKey(new ProductManager(), "id", package_id, gamePackage);
		return gamePackage;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
		product = null;
	}

	public Product getProduct() {
		product = foreignKey(new ProductManager(), "id", product_id, product);
		return product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setPackage_id(line.getInt("package_id"));
			this.setProduct_id(line.getInt("product_id"));
			this.setQuantity(line.getInt("quantity"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
