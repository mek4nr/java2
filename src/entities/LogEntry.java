package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogEntry  extends AbstractEntity {
	private Integer id;

	private Integer user_id;
	private User user;

	private Integer contentType_id;
	private ContentType contentType;

	private Integer object_id;
	private String object_repr;

	private Integer actionTag_id;
	private ActionTag actionTag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getContentType_id() {
		return contentType_id;
	}

	public void setContentType_id(Integer contentType_id) {
		this.contentType_id = contentType_id;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public Integer getObject_id() {
		return object_id;
	}

	public void setObject_id(Integer object_id) {
		this.object_id = object_id;
	}

	public String getObject_repr() {
		return object_repr;
	}

	public void setObject_repr(String object_repr) {
		this.object_repr = object_repr;
	}

	public Integer getActionTag_id() {
		return actionTag_id;
	}

	public void setActionTag_id(Integer actionTag_id) {
		this.actionTag_id = actionTag_id;
	}

	public ActionTag getActionTag() {
		return actionTag;
	}

	public void setActionTag(ActionTag actionTag) {
		this.actionTag = actionTag;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setActionTag_id(line.getInt("actionTag_id"));
			this.setContentType_id(line.getInt("contentType_id"));
			this.setId(line.getInt("id"));
			this.setObject_id(line.getInt("object_id"));
			this.setUser_id(line.getInt("user_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
