package entities;

import java.sql.ResultSet;

public interface IEntity {
	void generateObjectFromQuery(ResultSet line);
}
