package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Group  extends AbstractEntity {
	private Integer id;
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setId(line.getInt("id"));
			this.setName(line.getString("name"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
