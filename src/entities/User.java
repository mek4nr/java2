package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class User  extends AbstractEntity {
	private Integer id;
	private String email;
	private String password;
	private Timestamp create_time;
	private ETitle title;
	private String firstName;
	private String lastName;
	private String phoneHome;
	private String phonePortable;
	private Boolean removed;
	private Boolean is_staff;
	private Boolean is_superuser;
	private Timestamp last_login;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}
	public ETitle getTitle() {
		return title;
	}
	public void setTitle(ETitle title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneHome() {
		return phoneHome;
	}
	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}
	public String getPhonePortable() {
		return phonePortable;
	}
	public void setPhonePortable(String phonePortable) {
		this.phonePortable = phonePortable;
	}
	public Boolean getRemoved() {
		return removed;
	}
	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}
	public Boolean getIs_staff() {
		return is_staff;
	}
	public void setIs_staff(Boolean is_staff) {
		this.is_staff = is_staff;
	}
	public Boolean getIs_superuser() {
		return is_superuser;
	}
	public void setIs_superuser(Boolean is_superuser) {
		this.is_superuser = is_superuser;
	}
	public Timestamp getLast_login() {
		return last_login;
	}
	public void setLast_login(Timestamp last_login) {
		this.last_login = last_login;
	}
	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCreate_time(line.getTimestamp("create_time"));
			this.setEmail(line.getString("email"));
			this.setFirstName(line.getString("firstName"));
			this.setId(line.getInt("id"));
			this.setIs_staff(line.getBoolean("is_staff"));
			this.setIs_superuser(line.getBoolean("is_superuser"));
			this.setLast_login(line.getTimestamp("last_login"));
			this.setLastName(line.getString("lastName"));
			this.setPassword(line.getString("password"));
			this.setPhoneHome(line.getString("phoneHome"));
			this.setPhonePortable(line.getString("phonePortable"));
			this.setRemoved(line.getBoolean("removed"));
			this.setTitle(ETitle.values()[line.getInt("title")]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
