package entities;

import java.io.Serializable;
import java.util.ArrayList;

import managers.AbstractManager;

public abstract class AbstractEntity implements IEntity, Serializable {
	
	public AbstractEntity() {
		super();
	}

	public <T extends IEntity> T foreignKey (AbstractManager<T> manager, String key, int keyContent, T content){
		if(content == null)
			content = manager.getByField(key, keyContent);
		System.out.println(content);
		
		return content;
	}
	
	public <T extends IEntity> ArrayList<T> manyToMany (AbstractManager<T> manager, String key, int keyContent, ArrayList<T> content){
		if(content == null)
			content = manager.getAllByField(key, keyContent);
		System.out.println(content);
		
		return content;
	}
}
