package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserGroups extends AbstractEntity {
	private Integer group_id;
	private Group group;

	private Integer user_id;
	private User user;

	public Integer getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setGroup_id(line.getInt("group_id"));
			this.setUser_id(line.getInt("user_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
