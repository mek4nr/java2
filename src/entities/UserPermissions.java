package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserPermissions extends AbstractEntity {
	private Integer permission_id;
	private Permission permission;

	private Integer user_id;
	private User user;

	public Integer getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(Integer permission_id) {
		this.permission_id = permission_id;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setPermission_id(line.getInt("permission_id"));
			this.setUser_id(line.getInt("user_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
