package entities;

import java.util.HashMap;
import java.util.Map;

public class Basket {
	private HashMap<ProductView, Integer> products = new HashMap<>();

	public HashMap<ProductView, Integer> getProducts() {
		return products;
	}

	public void addToBasket(ProductView product, int qty){
		if(product == null)
			return ;
		
		boolean found = false;
		
		for(Map.Entry<ProductView, Integer> p : products.entrySet()) {
		    if(p.getKey().getProduct_id() == product.getProduct_id()){
		    	found = true;
		    	products.put(p.getKey(), p.getValue()+qty);
		    }
		}
		
		if(!found){
			products.put(product, qty);
		}
	}
	
	public void setToBasket(ProductView product, int qty){
		if(product == null)
			return ;
		
		if(qty <= 0)
			removeToBasket(product);
		
		boolean found = false;
		
		for(Map.Entry<ProductView, Integer> p : products.entrySet()) {
		    if(p.getKey().getProduct_id() == product.getProduct_id()){
		    	found = true;
		    	products.put(p.getKey(), qty);
		    }
		}
		
		if(!found){
			products.put(product, qty);
		}
	}
	
	
	@Override
	public String toString() {
		String content = "";
		for(Map.Entry<ProductView, Integer> p : products.entrySet()) {
		    content += String.format("%s : %s \n", p.getKey(), p.getValue());
		}
		return content;
	}
	
	public void removeToBasket(ProductView product){
		if(product == null)
			return ;
		
		for(Map.Entry<ProductView, Integer> p : products.entrySet()) {
		    if(p.getKey().getProduct_id() == product.getProduct_id()){
	    		products.remove(p.getKey());
		    }
		}
	}
	
	public void removeToBasket(ProductView product, int qty){
		if(product == null)
			return ;
		
		boolean found = false;
		
		for(Map.Entry<ProductView, Integer> p : products.entrySet()) {
		    if(p.getKey().getProduct_id() == product.getProduct_id()){
		    	found = true;
		    	Integer tmpQty = p.getValue()-qty;
		    	if(tmpQty <= 0)
		    		products.remove(p.getKey());
		    	else
		    		products.put(p.getKey(), tmpQty);
		    }
		}
		
		if(!found){
			products.put(product, qty);
		}
	}
}
