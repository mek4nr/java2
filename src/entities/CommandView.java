package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import managers.CommandManager;
import managers.ProductManager;
import managers.ProductViewManager;

public class CommandView extends AbstractEntity {
	private Integer command_id;
	private Command command;
	private Integer product_id;
	private ProductView productView;
	private Product product;
	
	private Integer quantity;
	private Float discount;
	private Float PTP;
	private Float VAT;
	private Float VAD;


	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	
	public Integer getCommand_id() {
		return command_id;
	}

	public void setCommand_id(Integer command_id) {
		this.command_id = command_id;
		command = null;
	}

	public Command getCommand() {
		command = foreignKey(new CommandManager(), "id", command_id, command);
		return command;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
		product = null;
		productView = null;
	}

	public Product getProduct() {
		product = foreignKey(new ProductManager(), "id", product_id, product);
		return product;
	}
	
	public ProductView getProductView() {
		productView = foreignKey(new ProductViewManager(), "id_product", product_id, productView);
		return productView;
	}

	public Float getPTP() {
		return PTP;
	}

	public void setPTP(Float pTP) {
		PTP = pTP;
	}

	public Float getVAT() {
		return VAT;
	}

	public void setVAT(Float vAT) {
		VAT = vAT;
	}

	public Float getVAD() {
		return VAD;
	}

	public void setVAD(Float vAD) {
		VAD = vAD;
	}
	
	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCommand_id(line.getInt("command_id"));
			this.setProduct_id(line.getInt("product_id"));
			this.setPTP(line.getFloat("PTP"));
			this.setQuantity(line.getInt("quantity"));
			this.setDiscount(line.getFloat("discount"));
			this.setVAD(line.getFloat("VAD"));
			this.setVAT(line.getFloat("VAT"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
