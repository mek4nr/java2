package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.jdt.internal.compiler.apt.util.ManyToMany;

import managers.CategoryManager;
import managers.DiscountManager;
import managers.ProductImageManager;
import managers.TaxesCategoryManager;

public class Product extends AbstractEntity {
	private Integer id;
	private String name;
	private String description;
	private Float price;

	private Integer category_id;
	private Category category;

	private Integer stock;
	private Date create_time;

	private Integer discount_id;
	private Discount discount;

	private Boolean removed;

	private Integer taxesCategory_id;
	private TaxesCategory taxesCategory;
	
	private ArrayList<ProductImages> productImages;

	public ArrayList<ProductImages> getProductImages() {
		productImages = manyToMany(new ProductImageManager(), "product_id", id, productImages);
		return productImages;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
		category = null;
	}

	public Category getCategory() {
		category =
				foreignKey(new CategoryManager(), "id", category_id, category);
		return category;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Integer getDiscount_id() {
		return discount_id;
	}

	public void setDiscount_id(Integer discount_id) {
		this.discount_id = discount_id;
		discount = null;
	}

	public Discount getDiscount() {
		discount =
				foreignKey(new DiscountManager(), "id", discount_id, discount);
		return discount;
	}

	public Boolean isRemoved() {
		return removed;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	public Integer getTaxesCategory_id() {
		return taxesCategory_id;
	}

	public void setTaxesCategory_id(Integer taxesCategory_id) {
		this.taxesCategory_id = taxesCategory_id;
		taxesCategory = null;
	}

	public TaxesCategory getTaxesCategory() {
		taxesCategory =
				foreignKey(
						new TaxesCategoryManager(),
						"id",
						taxesCategory_id,
						taxesCategory);
		return taxesCategory;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCategory_id(line.getInt("category_id"));
			this.setCreate_time(line.getDate("create_time"));
			this.setDescription(line.getString("description"));
			this.setDiscount_id(line.getInt("discount_id"));
			this.setName(line.getString("name"));
			this.setId(line.getInt("id"));
			this.setPrice(line.getFloat("price"));
			this.setRemoved(line.getBoolean("removed"));
			this.setStock(line.getInt("stock"));
			this.setTaxesCategory_id(line.getInt("taxesCategory_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
