package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import managers.TaxeManager;
import managers.TaxesCategoryManager;

public class TaxesCategoryList  extends AbstractEntity {
	private Integer taxesCategory_id;
	private TaxesCategory taxesCategory;

	private Integer taxe_id;
	private Taxe taxe;

	public Integer getTaxe_id() {
		return taxe_id;
	}

	public void setTaxe_id(Integer taxe_id) {
		this.taxe_id = taxe_id;
	}
	
	public Taxe getTaxe() {
		taxe = foreignKey(new TaxeManager(), "id", taxe_id, taxe);
		return taxe;
	}

	public Integer getTaxesCategory_id() {
		return taxesCategory_id;
	}

	public void setTaxesCategory_id(Integer taxesCategory_id) {
		this.taxesCategory_id = taxesCategory_id;
		taxesCategory = null;
	}
	
	public TaxesCategory getTaxesCategory() {
		taxesCategory = foreignKey(new TaxesCategoryManager(), "id", taxesCategory_id, taxesCategory);
		return taxesCategory;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setTaxe_id(line.getInt("taxe_id"));
			this.setTaxesCategory_id(line.getInt("taxesCategory_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
