package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import managers.UserManager;

public class Adress extends AbstractEntity {
	private Integer id;
	private Integer user_id;
	private User user;
	private String adress1;
	private String adress2;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private Timestamp create_time;
	private Boolean removed;

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
		user = null;
	}

	public User getUser() {
		user = foreignKey(new UserManager(), "id", user_id, user);
		return user;
	}

	public String getAdress1() {
		return adress1;
	}

	public void setAdress1(String adress1) {
		this.adress1 = adress1;
	}

	public String getAdress2() {
		return adress2;
	}

	public void setAdress2(String adress2) {
		this.adress2 = adress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Timestamp getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}

	public Boolean isRemoved() {
		return removed;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setAdress1(line.getString("adress1"));
			this.setAdress2(line.getString("adress2"));
			this.setCity(line.getString("city"));
			this.setCountry(line.getString("country"));
			this.setCreate_time(line.getTimestamp("create_time"));
			this.setId(line.getInt("id"));
			this.setPostalCode(line.getString("postalCode"));
			this.setRemoved(line.getBoolean("removed"));
			this.setState(line.getString("state"));
			this.setUser_id(line.getInt("user_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
