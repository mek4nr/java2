package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import managers.CommandManager;
import managers.ProductManager;

public class CommandProducts extends AbstractEntity {
	private Integer command_id;
	private Command command;

	private Integer product_id;
	private Product product;

	private Integer quantity;
	private Float price;
	private Float discount;

	public Integer getCommand_id() {
		return command_id;
	}

	public void setCommand_id(Integer command_id) {
		this.command_id = command_id;
		command = null;
	}

	public Command getCommand() {
		command = foreignKey(new CommandManager(), "id", command_id, command);
		return command;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
		product = null;
	}

	public Product getProduct() {
		product = foreignKey(new ProductManager(), "id", product_id, product);
		return product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setCommand_id(line.getInt("command_id"));
			this.setDiscount(line.getFloat("discount"));
			this.setPrice(line.getFloat("price"));
			this.setProduct_id(line.getInt("product_id"));
			this.setQuantity(line.getInt("quantity"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
