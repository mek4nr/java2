package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import managers.AdressManager;
import managers.StatusManager;
import managers.UserManager;

public class Command extends AbstractEntity {

	private Integer id;
	private String seed;

	private Integer user_id;
	private User user;

	private Integer delivery_adress_id;
	private Adress deliveryAdress;

	private Integer billing_adress_id;
	private Adress billingAdress;

	private Integer status_id;
	private Status status;

	private Timestamp create_time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
		user = null;
	}

	public User getUser() {
		user = foreignKey(new UserManager(), "id", user_id, user);
		return user;
	}

	public Integer getDelivery_adress_id() {
		return delivery_adress_id;
	}

	public void setDelivery_adress_id(Integer delivery_adress_id) {
		this.delivery_adress_id = delivery_adress_id;
		deliveryAdress = null;
	}

	public Adress getDeliveryAdress() {
		deliveryAdress = foreignKey(new AdressManager(), "id", delivery_adress_id, deliveryAdress);
		return deliveryAdress;
	}

	public Integer getBilling_adress_id() {
		return billing_adress_id;
	}

	public void setBilling_adress_id(Integer billing_adress_id) {
		this.billing_adress_id = billing_adress_id;
	}

	public Adress getBillingAdress() {
		billingAdress = foreignKey(new AdressManager(), "id", billing_adress_id, billingAdress);
		return billingAdress;
	}

	public Integer getStatus_id() {
		return status_id;
	}

	public void setStatus_id(Integer status_id) {
		this.status_id = status_id;
		status = null;
	}

	public Status getStatus() {
		status = foreignKey(new StatusManager(), "id", status_id, status);
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Timestamp getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setBilling_adress_id(line.getInt("billing_adress_id"));
			this.setCreate_time(line.getTimestamp("create_time"));
			this.setDelivery_adress_id(line.getInt("delivery_adress_id"));
			this.setId(line.getInt("id"));
			this.setSeed(line.getString("seed"));
			this.setStatus_id(line.getInt("status_id"));
			this.setUser_id(line.getInt("user_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
