package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupPermissions  extends AbstractEntity {
	private Integer permission_id;
	private Permission permission;

	private Integer group_id;
	private Group group;

	public Integer getPermission_id() {
		return permission_id;
	}

	public void setPermission_id(Integer permission_id) {
		this.permission_id = permission_id;
	}

	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Integer getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Integer group_id) {
		this.group_id = group_id;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setGroup_id(line.getInt("group_id"));
			this.setPermission_id(line.getInt("permission_id"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
