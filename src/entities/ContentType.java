package entities;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContentType extends AbstractEntity {
	private Integer id;
	private String model;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setId(line.getInt("id"));
			this.setModel(line.getString("model"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
