package entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class Discount extends AbstractEntity {
	private Integer id;
	private String name;
	private Float percent;
	private Timestamp limiteDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPercent() {
		return percent;
	}

	public void setPercent(Float percent) {
		this.percent = percent;
	}

	public Timestamp getLimiteDate() {
		return limiteDate;
	}

	public void setLimiteDate(Timestamp limiteDate) {
		this.limiteDate = limiteDate;
	}

	@Override
	public void generateObjectFromQuery(ResultSet line) {
		try {
			this.setId(line.getInt("id"));
			this.setLimiteDate(line.getTimestamp("limiteDate"));
			this.setName(line.getString("name"));
			this.setPercent(line.getFloat("percent"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
