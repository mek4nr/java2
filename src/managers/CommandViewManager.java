package managers;

import entities.CommandView;

public class CommandViewManager extends AbstractManager<CommandView> {
	public CommandViewManager() {
		super(new CommandView());
	}
}
