package managers;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import entities.CommandProducts;
import services.ConnectionBDD;

public class CommandProductsManager extends AbstractManager<CommandProducts> {

	public CommandProductsManager() {
		super(new CommandProducts());
	}
	
	public void insert(CommandProducts a) {

		String query = "INSERT INTO " + tableName + ""
				+ "(`command_id`, `product_id`, `quantity`) "
				+ "VALUES (? , ? , ? );";

		try {
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);

			preparedStatement.setInt(1, a.getCommand_id());
			preparedStatement.setInt(2, a.getProduct_id());
			preparedStatement.setInt(3, a.getQuantity());

			System.out.println(preparedStatement.toString());

			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
