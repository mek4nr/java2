package managers;

import entities.Taxe;

public class TaxeManager extends AbstractManagerWithName<Taxe> {
	public TaxeManager() {
		super(new Taxe());
	}
}
