package managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Command;
import services.ConnectionBDD;

public class CommandManager extends AbstractManager<Command> {
	public CommandManager() {
		super(new Command());
	}

	public Command insert(Command a) {

		String query = "INSERT INTO " + tableName + ""
				+ "(`user_id`, `seed`, `delivery_adress_id`, `billing_adress_id`, `status_id`) "
				+ "VALUES (? , ? , ? , ? , ? );";

		try {
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);

			preparedStatement.setInt(1, a.getUser_id());
			preparedStatement.setString(2, a.getSeed());
			preparedStatement.setInt(3, a.getDelivery_adress_id());
			preparedStatement.setInt(4, a.getBilling_adress_id());
			preparedStatement.setInt(5, a.getStatus_id());

			System.out.println(preparedStatement.toString());

			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					a.setId(generatedKeys.getInt(1));
				}
				else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return a;
	}
}
