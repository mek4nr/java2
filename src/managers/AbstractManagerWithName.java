package managers;

import java.util.ArrayList;

import entities.IEntity;

public abstract class AbstractManagerWithName<T extends IEntity> extends AbstractManager<T> {
	protected final String queryGetByName;
	
	public AbstractManagerWithName(T obj) {
		super(obj);
		queryGetByName = String.format("SELECT * FROM %s WHERE name like ?", tableName);
	}
	
	public ArrayList<T> getByName(String name)
	{
		return getByQuery(queryGetByName, name);
	}
}
