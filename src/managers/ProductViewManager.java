package managers;

import java.util.ArrayList;

import entities.ProductView;

public class ProductViewManager extends AbstractManager<ProductView> {
	
	String queryByName = "SELECT * FROM ProductView WHERE product_id in (SELECT id FROM Product as p where p.name like ?)";
	
	public ProductViewManager() {
		super(new ProductView());
	}
	
	public ArrayList<ProductView> getProductByName(String name){
		return this.getByQuery(queryByName, "%" + name + "%");
	}
}
