package managers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordManager {

	/**
	 * Hashes a password with the MD5 algorithm
	 * 
	 * @author Guillaume
	 * @param passwordToHash
	 * @return Ashed password as a String
	 */
	public static String code(String passwordToHash) {
		String codedPassword = null;

		try {
			// Creates MessageDigest instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");

			// Adds password bytes to digest
			md.update(passwordToHash.getBytes());

			// Gets the hash's bytes
			byte[] bytes = md.digest();
			// This bytes[] has bytes in decimal format

			// Converts it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer
						.toString((bytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}

			// Gets complete hashed password in hex format
			codedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return codedPassword;
	}

}
