package managers;

import entities.Status;

public class StatusManager extends AbstractManager<Status> {
	public StatusManager() {
		super(new Status());
	}
}
