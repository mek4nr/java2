package managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Adress;
import services.ConnectionBDD;

public class AdressManager extends AbstractManager<Adress> {

	public AdressManager() {
		super(new Adress());
	}

	public Adress insert(Adress a) {

		String query = "INSERT INTO " + tableName + ""
				+ "(`user_id`, `adress1`, `adress2`, `city`, `state`, `country`, `postalCode`) "
				+ "VALUES (? , ? , ? , ? , ? , ? , ? );";

		try {
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);

			preparedStatement.setInt(1, a.getUser_id());
			preparedStatement.setString(2, a.getAdress1());
			preparedStatement.setString(3, a.getAdress2());
			preparedStatement.setString(4, a.getCity());
			preparedStatement.setString(5, a.getState());
			preparedStatement.setString(6, a.getCountry());
			preparedStatement.setString(7, a.getPostalCode());

			System.out.println(preparedStatement.toString());

			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					a.setId(generatedKeys.getInt(1));
				}
				else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return a;
	}
}
