package managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.Product;
import services.ConnectionBDD;

public class ProductManager extends AbstractManagerWithName<Product> {
	public ProductManager() {
		super(new Product());
	}
	
	public Product insert(Product a) {

		String query = "INSERT INTO " + tableName + ""
				+ "(`name`, `description`, `price`, `stock`, `discount_id`, `taxesCategory_id`, `category_id`) "
				+ "VALUES (? , ? , ? , ? , ? , ? , ? );";

		try {
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);

			preparedStatement.setString(1, a.getName());
			preparedStatement.setString(2, a.getDescription());
			preparedStatement.setFloat(3, a.getPrice());
			preparedStatement.setInt(4, a.getStock());
			if(a.getDiscount_id() == null)
				preparedStatement.setNull(5, java.sql.Types.INTEGER);
			else
				preparedStatement.setInt(5, a.getDiscount_id());
			
			preparedStatement.setInt(6, a.getTaxesCategory_id());
			preparedStatement.setInt(7, a.getCategory_id());

			System.out.println(preparedStatement.toString());

			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					a.setId(generatedKeys.getInt(1));
				}
				else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return a;
	}
}
