package managers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.R;

public class CookieManager {

	/**
	 * Formats the cookie value from the user's ID and ashed password
	 * 
	 * @author Guillaume
	 * @param userID
	 * @param userPassword
	 * @return String according to template "ID":"ashed password"
	 */
	public static String formatCookie(
			int userID,
			String userPassword) {
		return String.format(
				"%d:%s",
				userID,
				userPassword);
	}

	/**
	 * Creates the cookie of the site
	 * 
	 * @author Guillaume
	 * @param cookieName
	 * @param userID
	 * @param userPassword
	 * @param response
	 */
	public static void createCookie(
			String cookieName,
			int userID,
			String userPassword,
			HttpServletResponse response) {
		Cookie userCookie = new Cookie(
				cookieName,
				formatCookie(userID, userPassword));

		userCookie.setMaxAge(60 * 60 * 24 * 15);
		response.addCookie(userCookie);
	}

	/**
	 * Deletes the cookie of the site
	 * 
	 * @author Guillaume
	 * @param cookieName
	 * @param request
	 * @param response
	 */
	public static void deleteCookie(
			String cookieName,
			HttpServletRequest request,
			HttpServletResponse response) {
		Cookie[] userCookie = request.getCookies();
		System.out.println(userCookie);

		for (Cookie c : userCookie) {
			if (c.getName().equals(cookieName)) {
				c.setMaxAge(0);
				response.addCookie(c);
			}
		}
	}

}
