package managers;

import entities.TaxesCategory;

public class TaxesCategoryManager extends AbstractManager<TaxesCategory> {
	public TaxesCategoryManager() {
		super(new TaxesCategory());
	}
}
