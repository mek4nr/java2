package managers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionManager {

	/**
	 * Sets an attribute to the session
	 * 
	 * @author Guillaume
	 * @param request
	 * @param attributeKey
	 * @param attributeValue
	 */
	public static void setAttribute(
			HttpServletRequest request,
			String attributeKey,
			Object attributeValue) {
		HttpSession session = request.getSession();
		session.setAttribute(attributeKey, attributeValue);
		System.out.println("Sessionadd");
	}

	/**
	 * Gets an attribute from the session
	 * 
	 * @author Guillaume
	 * @param request
	 * @param attributeKey
	 * @return The object stored or null
	 */
	public static Object getAttribute(
			HttpServletRequest request,
			String attributeKey) {
		HttpSession session = request.getSession();
		return session.getAttribute(attributeKey);
	}

	/**
	 * Invalidates the session
	 * 
	 * @author Guillaume
	 * @param request
	 */
	public static void deleteSession(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
	}

	public static void setFlashMessage(
			HttpServletRequest request,
			String attributeKey,
			String text) {
		HttpSession session = request.getSession();
		session.setAttribute(String.format("flash_%s", attributeKey), text);
	}
	
	public static void setFlashMessage(HttpServletRequest request, String attributeKey, ArrayList<String> text){
		HttpSession session = request.getSession();
		session.setAttribute(String.format("flash_%s", attributeKey), text);
	}
	
	public static ArrayList<String> getAllFlashMessage(HttpServletRequest request, String attributeKey){
		HttpSession session = request.getSession();
		ArrayList<String> s = (ArrayList<String>) session.getAttribute(String.format("flash_%s", attributeKey));
		session.removeAttribute(String.format("flash_%s", attributeKey));
		return s;
	}
	
	public static String getFlashMessage(HttpServletRequest request, String attributeKey){
		HttpSession session = request.getSession();
		String s =
				(String) session
						.getAttribute(String.format("flash_%s", attributeKey));
		session.removeAttribute(String.format("flash_%s", attributeKey));
		return s;
	}
}
