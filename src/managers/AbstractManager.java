package managers;

import java.lang.reflect.Constructor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import entities.IEntity;
import services.ConnectionBDD;

public abstract class AbstractManager<T extends IEntity>{
	protected Class<T> c;
	protected String tableName;
	protected final String queryGetById;
	protected final String queryGetAll;
	protected final String queryGetByRow;

	
	public AbstractManager(T obj)
	{
		c = (Class<T>) obj.getClass();
		tableName = c.getSimpleName();
		
		queryGetById = String.format("SELECT * FROM %s WHERE id = ?", tableName);
		queryGetAll = String.format("SELECT * FROM %s", tableName);
		queryGetByRow = queryGetAll + " WHERE %s = ?";
	}
	
	//TODO : test this function, could do all the job faster, only work for table 
	public ArrayList<T> getByQuery(String query, Object...params)
	{
		ArrayList<T> queryResponses = null;
		try 
		{
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);
			
			if(params != null)
			{
				int cmp = 0;
				
				for(Object o : params) 
				{
					cmp++;
					preparedStatement.setObject(cmp, o);
				}
			}
			
			System.out.println(preparedStatement.toString());
			
			ResultSet result = preparedStatement.executeQuery();
			
			if (result.isBeforeFirst())
			{
				queryResponses = new ArrayList<>();
				Constructor<T> co = c.getConstructor();
				while(result.next()){
					T t = co.newInstance();
					t.generateObjectFromQuery(result);
					queryResponses.add(t);
				}
			}
		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally {
			ConnectionBDD.closeConnection();
		}
		System.out.println(queryResponses);
		
		return queryResponses;
	}
	
	public ArrayList<T> getAll()
	{
		return getByQuery(queryGetAll);
	}
	
	public T getById(int id){
		ArrayList<T> ret = getByQuery(queryGetById, id);
		return (ret != null && ret.size() > 0) ? ret.get(0) : null;
	}
	
	public ArrayList<T> getAllByField(String row, Object content){
		return getByQuery(String.format(queryGetByRow, row), content);
	}
	
	public T getByField(String row, Object content){
		ArrayList<T> ret = getByQuery(String.format(queryGetByRow, row), content);
		return (ret != null && ret.size() > 0) ? ret.get(0) : null;
	}
}
