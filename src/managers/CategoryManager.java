package managers;

import entities.Category;

public class CategoryManager extends AbstractManager<Category> {
	public CategoryManager() {
		super(new Category());
	}
}
