package managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entities.User;
import services.ConnectionBDD;

public class UserManager extends AbstractManager<User> {
	public UserManager() {
		super(new User());
	}
	
	public User insert(User u) {
		
		String query = "INSERT INTO " + tableName + ""
				+ "(`email`, `password`, `title`, `firstName`, `lastName`, `phoneHome`, `phonePortable`, `is_staff`, `is_superuser`) "
				+ "VALUES (? , ? , ? , ? , ? , ? , ? , false, false);";
		
		try {
			PreparedStatement preparedStatement = ConnectionBDD.prepareStatement(query);
			
			preparedStatement.setString(1, u.getEmail());
			preparedStatement.setString(2, u.getPassword());
			preparedStatement.setInt(3, u.getTitle().ordinal());
			preparedStatement.setString(4, u.getFirstName());
			preparedStatement.setString(5, u.getLastName());
			preparedStatement.setString(6, u.getPhoneHome());
			preparedStatement.setString(7, u.getPhonePortable());
			
			System.out.println(preparedStatement.toString());
			
			int affectedRows = preparedStatement.executeUpdate();

			if (affectedRows == 0) {
				throw new SQLException("Creating user failed, no rows affected.");
			}

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					u.setId(generatedKeys.getInt(1));
				}
				else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}
}
