package managers;

import entities.Discount;

public class DiscountManager extends AbstractManager<Discount> {
	public DiscountManager() {
		super(new Discount());
	}
}
