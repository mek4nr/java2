<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, services.R, entities.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">

<title>Clever Games</title>

<!-- Loading third party fonts -->
<link
	href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|"
	rel="stylesheet" type="text/css">
<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="fonts/lineo-icon/style.css" rel="stylesheet" type="text/css">

<!-- Loading main css file -->
<link rel="stylesheet" href="style.css">

<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>
<body class="slider-collapse">

	<div id="site-content">
		<%@ include file="include/header.jsp"%>


		<main class="main-content">
		<div class="container">
			<div class="page">
				<%
					ProductView productView = (ProductView) request.getAttribute(R.productView);
					if (productView != null) {
						
						ArrayList<ProductImages> productImages = productView.getProduct().getProductImages();
						if(productImages == null)
							productImages = ProductImages.getDefault(productView.getProduct_id());
						
				%>

				<div class="entry-content">
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<div class="product-images">
								<figure class="large-image"> <a
									href="<%=productImages.get(0).getSrc()%>"><img
									src="<%=productImages.get(0).getSrc()%>" alt=""></a> </figure>
								<div class="thumbnails">
									<%
										for (int i = 1; i < productImages.size(); i++) {
									%>
									<a href="<%=productImages.get(i).getSrc()%>"><img
										src="<%=productImages.get(i).getSrc()%>" alt=""></a>
									<%
										}
									%>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-8">
							<h2 class="entry-title"><%=productView.getProduct().getName()%></h2>
							<small class="price"> <%
 	if (productView.getProduct().getDiscount() != null) {
 %> <strike>$<%=productView.getVAT()%></strike>&emsp;-<%=productView.getProduct().getDiscount().getPercent()%>%&emsp;$<%=productView.getVAD()%>
								<%
									} else {
								%> $<%=productView.getVAT()%> <%
 	}
 %>
							</small>

							<p><%=productView.getProduct().getDescription()%></p>

							<div class="addtocart-bar">
								<form method="post"
									action="<%=R.getAddBasketLink(productView.getProduct_id())%>">
									<label for="<%=productView.getProduct_id()%>">Quantity</label>
									<select id="<%=productView.getProduct_id()%>"
										name="<%=R.quantity%>">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select> <input type="submit" value="Add to cart">
								</form>

								<div class="social-links square">
									<strong>Share</strong> <a href="#" class="facebook"><i
										class="fa fa-facebook"></i></a> <a href="#" class="twitter"><i
										class="fa fa-twitter"></i></a> <a href="#" class="google-plus"><i
										class="fa fa-google-plus"></i></a> <a href="#" class="pinterest"><i
										class="fa fa-pinterest"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%
					} else {
				%>

				<!--  Missing product here -->
				Aucun produit trouvé

				<%
					}
				%>



			</div>
		</div>
		<!-- .container --> </main>
		<!-- .main-content -->


		<%@ include file="include/footer.jsp"%>
	</div>

	<div class="overlay"></div>
</body>
</html>