<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, services.R, entities.*, java.util.HashMap, java.util.Map"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">

<title>Clever Games</title>

<!-- Loading third party fonts -->
<link
	href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|"
	rel="stylesheet" type="text/css">
<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="fonts/lineo-icon/style.css" rel="stylesheet" type="text/css">

<!-- Loading main css file -->
<link rel="stylesheet" href="style.css">

<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>
<body class="slider-collapse">

	<div id="site-content">
		<%@ include file="include/header.jsp"%>
		<main class="main-content">
				<div class="container">
					<div class="page">
					<% 
					ArrayList<String> errorsAdress = SessionManager.getAllFlashMessage(request, R.errorAdress);
					if(errorsAdress != null){ %>
						<div class="cart-adress">
						Erreur lors de la commande :
						<ul>
						<% for(String errorAdress : errorsAdress){ %>
						<li><%=errorAdress %></li>
						<% } %>
						</ul>
						</div>
					<% } %>
						<table class="cart">
							<thead>
								<tr>
									<th class="product-name">Product Name</th>
									<th class="product-price">Price</th>
									<th class="product-qty">Quantity</th>
									<th class="product-total">Total</th>
									<th class="action"></th>
								</tr>
							</thead>
							<tbody>
							
							<%
		
								float total = 0;
		
								if (basket != null) {
									for(Map.Entry<ProductView, Integer> p : basket.getProducts().entrySet()) {
										double price = p.getKey().getVAD();
										double quantity = p.getValue();
										total += price * quantity;
										ArrayList<ProductImages> productImages = p.getKey().getProduct().getProductImages();
										if(productImages == null)
											productImages = ProductImages.getDefault(p.getKey().getProduct_id());
							%>
								<tr>
									<td class="product-name">
										<div class="product-thumbnail">
											<img src="<%=productImages.get(0).getSrc() %>" alt="">
										</div>
										<div class="product-detail">
											<h3 class="product-title"><%=p.getKey().getProduct().getName() %></h3>
											<p><%=p.getKey().getProduct().getDescription() %></p>
										</div>
									</td>
									<td class="product-price">$<%=price %></td>
									<td class="product-qty"><form action="<%=R.getSetBasketLink(p.getKey().getProduct_id())%>" method="post"><input onchange="submit();" type="number" name="<%=R.quantity%>" value="<%=quantity %>"></form></td>
									<td class="product-total">$<%=price*quantity %></td>
									<td class="action"><a href="<%=R.getRemoveBasketLink(p.getKey().getProduct_id())%>"><i class="fa fa-times"></i></a></td>
								</tr>
								
								<%
										}
									}
								%>
							</tbody>
						</table> <!-- .cart -->
						<form method="post" action="<%=R.getCreateCommandLink()%>">
							<div class="cart-adress">
								<h1>Adresse de livraison : </h1>
								
								<div>
									<label for="adress1">Adresse 1 : </label><input name="<%=R.formAdressAdress1 %>" class="xxl" id="adress1" type="text" /></div>
								<div>
									<label for="adress2">Adresse 2 : </label><input name="<%=R.formAdressAdress2 %>" class="xxl" id="adress2" type="text" /></div>
								<div>
									<label for="city">Ville : </label><input name="<%=R.formAdressCity %>"  id="city" type="text" /></div>
								<div>
									<label for="postalCode">Code postal : </label><input name="<%=R.formAdressPostalCode %>" id="postalCode" type="text" maxlength="6" /></div>
								<div>
									<label for="state">Provinces : </label>
									<select id="state" name="<%=R.formAdressState %>" >
										<option value="alberta">Alberta</option>
										<option value="colombie-britanique">Colombie-Britannique</option>
										<option value="ile Du Prince Edouard">Ile du prince Edouard</option>
										<option value="manitoba">Manitoba</option>
										<option value="nouveau-brunswick">Nouveau-Brunswick</option>
										<option value="nouvelle-ecosse">Nouvelle-Ecosse</option>
										<option value="ontario">Ontario</option>
										<option value="quebec">Québec</option>
										<option value="saskatchewan">Saskatchewan</option>
										<option value="terre-neuve et labrador ">Terre Neuve et Labrador</option>
									</select>
								</div>
								<div><label for="country">Pays : </label><input name="<%=R.formAdressCountry %>"  id="country" type="text" /></div>
								
							</div> <!-- .cart-adress -->

							<div class="cart-total">
								<p><strong>Subtotal:</strong> $<%=total %></p>
								<p><strong>Shipment:</strong> $15.00</p>
								<p class="total"><strong>Total</strong><span class="num">$<%=(total+15.00) %></span></p>
								<p>
									<a href="<%=R.getProductsLink() %>" class="button muted">Continue Shopping</a>
									<input type="submit" value="Finalize and pay" class="button">
								</p>
							</div> <!-- .cart-total -->
						</form>
					</div>
				</div> <!-- .container -->
			</main> <!-- .main-content -->

		<%@ include file="include/footer.jsp"%>
	</div>

	<div class="overlay"></div>

	



</body>
</html>