<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, services.R, entities.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">

<title>Clever Games</title>

<!-- Loading third party fonts -->
<link
	href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|"
	rel="stylesheet" type="text/css">
<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="fonts/lineo-icon/style.css" rel="stylesheet" type="text/css">

<!-- Loading main css file -->
<link rel="stylesheet" href="style.css">

<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>
<body class="slider-collapse">

	<div id="site-content">
		<%@ include file="include/header.jsp"%>


		<main class="main-content">
		<div class="container">
			<div class="page">
				<section>

				<div class="product-list">

					<%
						ArrayList<ProductView> products =
								(ArrayList<ProductView>) request
										.getAttribute(R.listProductView);

						int compteur = 0;

						if (products != null) {
							for (ProductView p : products) {
								compteur++;
								ArrayList<ProductImages> productImages = p.getProduct().getProductImages();
								if(productImages == null)
									productImages = ProductImages.getDefault(p.getProduct_id());
					%>
					<div class="product <%=p.getProduct().getCategory().getName()%>">
						<div class="inner-product">
							<div class="figure-image">
								<a href="<%=R.getProductLink(p.getProduct_id())%>"><img src="<%=productImages.get(0).getSrc() %>"
									alt="Game <%=compteur%>"></a>
							</div>
							<h3 class="product-title">
								<a href="<%=R.getProductLink(p.getProduct_id())%>"><%=p.getProduct().getName()%></a>
							</h3>
							<small class="price"> <%
							 	if (p.getProduct().getDiscount() != null) {
							 %> <strike>$<%=p.getVAT()%></strike>&emsp;-<%=p.getProduct().getDiscount().getPercent()%>%&emsp;$<%=p.getVAD()%>
								<%
							 	} else {
							 %> $<%=p.getVAT()%> <%
							 	}
							 %>
							</small>
							<p><%=p.getProduct().getDescription()%></p>
							<a href="<%=R.getAddBasketLink(p.getProduct_id())%>"
								class="button">Ajouter au panier</a> <a
								href="<%=R.getProductLink(p.getProduct_id())%>"
								class="button muted">Détails</a>
						</div>
					</div>
					<!-- .product -->

					<%
						}
						}
					%>


				</div>
				<!-- .product-list --> </section>
			</div>
		</div>
		<!-- .container --> </main>
		<!-- .main-content -->

		<%@ include file="include/footer.jsp"%>
	</div>

	<div class="overlay"></div>

</body>
</html>