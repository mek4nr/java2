<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, services.R, entities.*, managers.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div class="site-header">
		<div class="container">
			<a href="<%=R.getHomeLink()%>" id="branding"> <img
				src="images/logo.png" alt="" class="logo">
				<div class="logo-text">
					<h1 class="site-title">Clever Games</h1>
					<small class="site-description">on vous les vend comme si
						c'était pour nous...</small>
				</div>
			</a>
			<!-- #branding -->

			<%
				Basket basket = (Basket) SessionManager.getAttribute(request, R.basket);
				int nb = 0;
				if (basket != null)
					nb = basket.getProducts().size();
			%>

			<div class="right-section pull-right">
				<a href="<%=R.getBasketLink()%>" class="cart"><i
					class="icon-cart"></i> <%=nb%> <%=nb > 1 ? "articles" : "article"%>
					dans le panier</a>
				<%
					// Checks session and cookies
					User visitor = (User) session.getAttribute(R.sessionUser);
					Cookie[] userCookies = request.getCookies();

					boolean isSiteCookie = false;
					Cookie siteCookie = null;

					if (userCookies != null) {
						for (Cookie c : userCookies) {
							if (c.getName().equals(R.cookieName)) {
								isSiteCookie = true;
								siteCookie = c;
							}
						}
					}
					// Session still active
					if (visitor != null) {
						out.print(visitor.getFirstName());
				%>
				<a href="SessionOutController" class="disconnect-session">Se
					déconnecter</a>
				<%
					} else if (isSiteCookie) {
						// Session is inactive but cookie exists
						ArrayList<User> users = (new UserManager()).getAll();
						int cmp = 0;
						boolean isFound = false;

						// Search the user corresponding to the cookie
						while (cmp < users.size() && !isFound) {
							if (CookieManager.formatCookie(users.get(cmp).getId(), users.get(cmp).getPassword())
									.equals(siteCookie.getValue())) {
								visitor = users.get(cmp);
								isFound = true;
							} else {
								cmp++;
							}
						}

						// Put the user in the session
						if (visitor != null) {
							SessionManager.setAttribute(request, R.sessionUser, visitor);
							out.print(visitor.getFirstName());
						}
				%>
				<a href="SessionOutController" class="disconnect-session">Se
					déconnecter</a>
				<%
					} else {
				%>
				<a href="#" class="login-button">Connexion/Inscription</a>
				<%
					}
				%>
			</div>
			<!-- .right-section -->

			<div class="main-navigation">
				<button class="toggle-menu">
					<i class="fa fa-bars"></i>
				</button>
				<ul class="menu">
					<li class="menu-item home current-menu-item"><a href="Home">Tous
							les produits</a></li>
					<%
						ArrayList<Category> categories = (ArrayList<Category>) request.getAttribute(R.listCategory);
						if (categories != null) {
							for (Category c : categories) {
					%>
					<li class="menu-item"><a href="#"
						data-category="<%=c.getName()%>"><%=c.getName()%></a></li>
					<%
						}
						}
					%>
				</ul>
				<!-- .menu -->
				<div class="search-form">
					<form action="<%=R.getProductsLink()%>">
						<label><img src="images/icon-search.png"></label> <input
							type="text" name="<%=R.nameProduct%>" placeholder="Search...">
						<input type="submit">
					</form>
				</div>
				<!-- .search-form -->

				<div class="mobile-navigation"></div>
				<!-- .mobile-navigation -->
			</div>
			<!-- .main-navigation -->
		</div>
		<!-- .container -->
	</div>
	<!-- .site-header -->

	<div class="auth-popup popup">
		<a href="#" class="close"><i class="fa fa-times"></i></a>
		<div class="row">
			<div class="col-md-6">
				<h2 class="section-title">Connexion</h2>
				<form action="AuthentificationController" method="post">
					<input type="text" placeholder="Username..."
						name="<%=R.authorizationLogin%>"> <input type="password"
						placeholder="Password..." name="<%=R.authorizationPassword%>")>
					<div class="inlineDiv">Se souvenir de moi</div>
					<input type="checkbox" name="<%=R.cookieName%>"
						class="inlineDiv checkedAdapt"> <input type="submit"
						value="Login">
				</form>
			</div>
			<!-- .column -->
			<div class="col-md-6">
				<h2 class="section-title">Créer un compte</h2>
				<form action="ProfileCreationController" method="post">
					<%
						String s = SessionManager.getFlashMessage(request, R.errorSubscribe);
						if (s != null) {
					%>
					<div class="form-error">
						<%=s%>
					</div>
					<%
						}
					%>
					<input type="text" placeholder="Prénom..."
						name="<%=R.authorizationFirstName%>" required> <input
						type="text" placeholder="Nom..."
						name="<%=R.authorizationLastName%>" required> <select
						name="<%=R.authorizationTitle%>" required>
						<option disabled selected value>-- Sélectionnez une
							option --</option>
						<option value="0">Homme</option>
						<option value="1">Femme</option>
					</select> <input type="email" placeholder="Courriel..."
						name="<%=R.authorizationLogin%>" required> <input
						type="password" placeholder="Mot de passe..."
						name="<%=R.authorizationPassword%>" required> <input
						type="text" placeholder="Téléphone domicile..."
						) name="<%=R.authorizationPhoneHome%>"> <input type="text"
						placeholder="Téléphone portable..."
						) name="<%=R.authorizationPhonePortable%>"> <input
						type="reset"> <input type="submit">
				</form>
			</div>
			<!-- .column -->
		</div>
		<!-- .row -->
	</div>
	<!-- .auth-popup -->
</body>
<script type="text/javascript">
	document
			.addEventListener(
					"DOMContentLoaded",
					function(event) {
						// Sets the menu button function
						var links = document.querySelectorAll(".menu-item > a");
						var products = document.querySelectorAll(".product");

						for ( var l in links) {
							links[l].onclick = function(e) {
								if (!e.target.parentElement.classList
										.contains("current-menu-item")) {
									// Sets the active menu button
									document
											.getElementsByClassName("current-menu-item")[0].classList
											.remove("current-menu-item");
									e.target.parentElement.classList
											.add("current-menu-item");
								}

								if (e.target.parentElement.classList
										.contains("home")) {
									for (var i = 0; i < products.length; i++) {
										products[i].style.display = "block";
									}
								} else {
									for (var i = 0; i < products.length; i++) {
										products[i].style.display = "none";
									}

									var categoryProducts = document
											.querySelectorAll("."
													+ e.target.dataset.category);

									for (var i = 0; i < categoryProducts.length; i++) {
										categoryProducts[i].style.display = "block";
									}
								}
							}
						}

						// Sets the login buttons
						var loginButtons = document
								.querySelectorAll(".login-button");

						for (var i = 0; i < loginButtons.length; i++) {
							loginButtons[i].onclick = function(e) {
								document.getElementsByClassName("auth-popup")[0].classList
										.add("active");
							}
						}

						// Sets the close connection menu button
						var loginClose = document
								.getElementsByClassName("close")[0];
						loginClose.onclick = function(e) {
							document.getElementsByClassName("auth-popup")[0].classList
									.remove("active");
						}
					});
</script>

</html>