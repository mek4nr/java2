<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="services.R"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<div class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<div class="widget">
						<h3 class="widget-title">Informations</h3>
						<ul class="no-bullet">
							<li><a href="#">Nous connaître</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div>
					<!-- .widget -->
				</div>
				<!-- column -->
				<div class="col-md-2">
					<div class="widget">
						<h3 class="widget-title">Mon compte</h3>
						<ul class="no-bullet">
							<li><a href="#" class="login-button">Connexion/Inscription</a></li>
							<li><a href="<%=R.getBasketLink()%>" class="cart">Panier</a></li>
							<li><a href="SessionOutController"
								class="disconnect-session">Déconnexion</a></li>
						</ul>
					</div>
					<!-- .widget -->
				</div>
				<!-- column -->
			</div>
			<!-- .row -->

			<div class="colophon">
				<div class="copy">Copyright 2017 Clever Games. Designed by
					Themezy. All rights reserved.</div>
				<div class="social-links square">
					<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
						class="fa fa-twitter"></i></a>
				</div>
				<!-- .social-links -->
			</div>
			<!-- .colophon -->
		</div>
		<!-- .container -->
	</div>
	<!-- .site-footer -->
</body>
</html>