<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList, services.R, entities.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">

<title>Clever Games</title>

<!-- Loading third party fonts -->
<link
	href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|"
	rel="stylesheet" type="text/css">
<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="fonts/lineo-icon/style.css" rel="stylesheet" type="text/css">

<!-- Loading main css file -->
<link rel="stylesheet" href="style.css">

<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

</head>
<body class="slider-collapse">

	<div id="site-content">
		<%@ include file="include/header.jsp"%>

		<main class="main-content">
		<div class="container">
			<div class="page">
				<div class="cart-adress">
					<form action="<%=R.getAddProductLink()%>" method="post">
						<div>
							<label for="name">Nom : </label><input
								name="<%=R.formProductName%>" class="xxl" id="name" type="text" />
						</div>
						<div>
							<label for="desc">Description : </label>
							<textarea name="<%=R.formProductDescription%>" class="xxl"
								id="desc"></textarea>
						</div>
						<div>
							<label for="price">Prix : </label><input
								name="<%=R.formProductPrice%>" id="price" type="text" />
						</div>
						<div>
							<label for="stock">Stock : </label><input
								name="<%=R.formProductStock%>" id="stock" type="text" />
						</div>
						<div>
							<label for="category">Category : </label><select
								name="<%=R.formProductCategory%>" id="category">
								<%
									for (Category category : categories) {
								%>
								<option value="<%=category.getId()%>"><%=category.getName()%></option>
								<%
									}
								%>
							</select>
						</div>

						<div>
							<label for="state">Discount : </label> <select id="state"
								name="<%=R.formProductDiscount%>">
								<option value="-1">No discount</option>
								<%
									ArrayList<Discount> discounts = (ArrayList<Discount>) request.getAttribute(R.listDiscount);
									for (Discount discount : discounts) {
								%>
								<option value="<%=discount.getId()%>"><%=discount.getName()%></option>
								<%
									}
								%>
							</select>
						</div>
						<div>
							<label for="country">Taxes : </label><select id="state"
								name="<%=R.formProductTaxe%>">
								<%
									ArrayList<TaxesCategory> taxes = (ArrayList<TaxesCategory>) request.getAttribute(R.listTaxe);
									for (TaxesCategory taxe : taxes) {
								%>
								<option value="<%=taxe.getId()%>"><%=taxe.getName()%></option>
								<%
									}
								%>
							</select>
						</div>
						<input type="submit" value="Ajouter" />
					</form>
				</div>
			</div>
		</div>
		<!-- .container --> </main>
		<!-- .main-content -->

		<%@ include file="include/footer.jsp"%>
	</div>

	<div class="overlay"></div>

</body>
</html>